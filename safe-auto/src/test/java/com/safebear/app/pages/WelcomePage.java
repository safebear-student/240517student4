package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 24/05/2017.
 */
public class WelcomePage {
    WebDriver driver;

    public WelcomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Login")
    WebElement loginLink1;

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    WebElement loginLink2;

    public Boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    public Boolean clickOnLogin(LoginPage loginPage) {
        loginLink1.click();
        return loginPage.checkCorrectPage();
    }
}
