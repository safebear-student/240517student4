package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 24/05/2017.
 */
public class Test01_Login extends BaseTest {


    @Test
    public void testLogin() {assertTrue(true);
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 click on the Login link and it loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser", "testing"));
    }
}
